
# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
RSpec.configure do |config|

  config.before(:all) do
    if ENV['api_key'] == nil
      puts "api_key missing in config.rb"
      raise StandardError, "api_key missing in config.rb"
    end
  end
  config.expect_with :rspec do |expectations|
    
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  #Will profile up to 10
  config.profile_examples = 2

  #comment out to run tests in order
  config.order = :random

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
end
