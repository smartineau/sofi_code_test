require_relative '../class/find_endpoint_class.rb'

find_endpoint_class = FindEndpointClass.new

=begin
   Test ID's from external DB's using their id to make sure mapping still correct
    IMDB is the only one who supports movies.

    Test find works with preselected keys
    Test find works by using internal id to get a list of external id's, then test each one and make sure all return the same

    For each test check the structure of the response. Are all keys we expect to see there, even if value is nil?
    Are there any keys we aren't expecting? Delete all keys and check length. If to many, spit out what they are.

=end

RSpec.describe 'Find API' do

  it 'succeeds with Bullitt from IMDB' do
    url = "#{ENV['base_url']}find/tt0062765?external_source=imdb_id&language=en-US&api_key=#{ENV['api_key']}"
    response = HTTParty.get(url)

    #check fields that should be blank are blank
    expect(response["tv_results"]).to eq([])
    expect(response["tv_episode_results"]).to eq([])
    expect(response["tv_season_results"]).to eq([])

    #check we did find the movie looking for
    expect(response["movie_results"][0]["original_title"]).to eq("Bullitt")
    expect(find_endpoint_class.check_required_movie_json_keys(response)).to eq(true)
    expect(find_endpoint_class.check_no_extra_movie_keys(response)).to eq(true)
  end

  #we keep a reference to the external id's, get that using internal id and verify
  #check that we can use interal id to get list of external id's, and verify they work
  it 'gets all external ids for a tv show' do
    url = "#{ENV['base_url']}tv/2490/external_ids?language=en-US&api_key=#{ENV['api_key']}"
    response = HTTParty.get(url)
    expect(response["freebase_id"]).to eq("/en/the_it_crowd")
    tv_show_ids = response

    #check imdb using key tied to record
    imdb_url = "#{ENV['base_url']}find/#{tv_show_ids["imdb_id"]}?external_source=imdb_id&language=en-US&api_key=#{ENV['api_key']}"
    imdb_response = HTTParty.get(imdb_url)
    expect(imdb_response["movie_results"]).to eq([])
    expect(imdb_response["tv_results"][0]["original_name"]).to eq("The IT Crowd")
  end

  it 'succeeds with The It Crowd from IMDB' do
    url = "#{ENV['base_url']}find/tt0487831?external_source=imdb_id&language=en-US&api_key=#{ENV['api_key']}"
    response = HTTParty.get(url)
    expect(response["tv_results"][0]["original_name"]).to eq("The IT Crowd")
    expect(find_endpoint_class.check_no_extra_tv_keys(response)).to eq(true)
    expect(find_endpoint_class.check_required_tv_json_keys(response)).to eq(true)
  end

  it 'succeeds with The IT Crowd from TVDB' do
    url = "#{ENV['base_url']}find/79216?external_source=tvdb_id&language=en-US&api_key=#{ENV['api_key']}"
    response = HTTParty.get(url)
    expect(response["tv_results"][0]["original_name"]).to eq("The IT Crowd")
    expect(response["movie_results"] && response["person_results"]).to eq([])
  end

  #failing test
  # it 'succeeds with The IT Crowd from tvrage' do
  #   url = "#{ENV['base_url']}find/8044?external_source=tvrage_id&language=en-US&api_key=#{ENV['api_key']}"
  #   response = HTTParty.get(url)
  #   expect(response["tv_results"][0]["original_name"]).to eq("The IT Crowd")
  #   expect(response["movie_results"] && response["person_results"]).to eq([])
  # end

=begin
  The tests for freebase_mid, freebase_id, and tvrage work, but are commented out because
  the services no longer exist or have moved.
=end

  # #freebase has moved to wikidata
  # it 'succeeds with The IT Crowd from freebase_mid' do
  #   url = "#{ENV['base_url']}find/m/08zvbj?external_source=freebase_mid&language=en-US&api_key=#{ENV['api_key']}"
  #   response = HTTParty.get(url)
  #   expect(response["tv_results"][0]["original_name"]).to eq("The IT Crowd")
  # end
  # #freebase has moved to wikidata
  # it 'succeeds with The IT Crowd from freebase_id' do
  #   url = "#{ENV['base_url']}find/en/the_it_crowd?external_source=freebase_id&language=en-US&api_key=#{ENV['api_key']}"
  #   response = HTTParty.get(url)
  #   expect(response["tv_results"][0]["original_name"]).to eq("The IT Crowd")
  # end
end
