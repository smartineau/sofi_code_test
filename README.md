# Setup #

### What is this repository for? ###
* SoFi code test
* This will test the open movie db api using Ruby with the Rspec gem
* Ruby https://www.ruby-lang.org/en/
* Rspec http://rspec.info/

### How do I get set up? ###

Homebrew is the recommended way to install Ruby if you don't have it already.
 Apple does ship with Ruby, but it lags updates and frequently causes issues.

1. Check for ruby with `ruby -v`, `brew install ruby` if you don't have.

2. After Ruby is installed we need the dependency manager, bundler.
    `gem install bundler`

3. `bundler install` Add dependencies in Gemfile.

### Testing Time ###

Tests are in *spec/test_find_api_spec.rb*.

API key is in *config.rb* 
`ENV['api_key'] ||= 'yourKeyHere'`

Tests can be run several ways.
`rspec` Will run and report back to command line.

`rake rspec_report:html` will run and save to html file, but not open.

`rake rspec_report:browser` will run everything, create an HTML page in the reports folder, and open the browser to show results.
 This file is in the gitignore, since I'm not saving results between runs.

 Environment variables have been added inside the *config.rb* file. 
 For a different api_key, api_version, or base_url, pass them in with the rake task.
 For Example: `api_version=4 rake rspec_report:browser` 

 Currently tests are running in a random order and printing two slowest in console. This can be changed in *spec_helper.rb*















