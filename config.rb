
#api key goes here. ||= allows us to set a default that will be used
#unless a new value is passed through command line.

ENV['api_key'] ||= nil
ENV['api_version'] ||= '3'
ENV['base_url'] ||= "https://api.themoviedb.org/#{ENV['api_version']}/"
