#move all dependencies in base class
require 'rspec'
require 'httparty'
require 'pry'
require_relative '../config.rb'

class BaseClass

#Move json key checks into base class, should be checking keys in all tests
#moving keys array into each class

#check for keys. If all present, return true. if not return missing key.
  #subclass cleans data for passing to super
  def check_for_json_keys(keys, response)
    keys.each do |key|
      if !response.has_key? key
        return key
      end
    end
    true
  end

  def check_no_extra_json_keys(keys, response)
    response_keys = response.keys
    keys.each do |key|
      response_keys.delete(key)
    end
    if response_keys.length == 0
      true
    else
      response_keys
    end
  end

end
