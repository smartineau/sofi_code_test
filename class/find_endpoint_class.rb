require_relative 'base_class'

class FindEndpointClass < BaseClass
  #move required json keys to a classwide constant
  REQUIRED_MOVIE_KEYS = ["adult", "backdrop_path", "genre_ids", "id", "original_language", "original_title", "overview", "release_date", "poster_path", "popularity", "title", "video", "vote_average", "vote_count"]

  REQUIRED_TV_KEYS = [ "backdrop_path", "first_air_date", "genre_ids", "id", "original_language", "original_name", "overview", "origin_country", "poster_path", "popularity", "name", "vote_average", "vote_count"]

  def check_required_movie_json_keys(response)
    check_for_json_keys(REQUIRED_MOVIE_KEYS, response["movie_results"][0])
  end

  def check_required_tv_json_keys(response)
    check_for_json_keys(REQUIRED_TV_KEYS, response["tv_results"][0])
  end

  def check_no_extra_movie_keys(response)
    check_no_extra_json_keys(REQUIRED_MOVIE_KEYS, response["movie_results"][0])
  end

  def check_no_extra_tv_keys(response)
    check_no_extra_json_keys(REQUIRED_TV_KEYS, response["tv_results"][0])
  end

end
